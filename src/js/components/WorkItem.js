import React, { PureComponent } from 'react';
import Project from './Project';
import Button from 'react-md/lib/Buttons/Button';
import Collapse from 'react-md/lib/Helpers/Collapse';

export default class WorkItem extends PureComponent {

    constructor(props) {
      super(props);

      this.state = { 
        collapsed: true,
        className: 'fa fa-angle-down'
      };
      this._toggleCollapse = this._toggleCollapse.bind(this);
    }

    _toggleCollapse() {
      let className;
      if(this.state.collapsed) {
        className = 'fa fa-angle-up';
      } else {
        className = 'fa fa-angle-down';
      }
      console.log(this.state.collapsed);
      this.setState({ 
        collapsed: !this.state.collapsed,
        className: className
      });
    }
    
    getWorkDates = () => {
        const startdate = this.props.workItemData.startDate;
        let enddate = null;
        if (this.props.workItemData.endDate !== '') {
          enddate = this.props.workItemData.endDate;
        } else {
          enddate = 'aujourd\'hui';
        }

        return <span className='startdate'>{startdate} - {enddate}</span>
    }

    getProjects = () => {
        let projects = [];

        projects.push(this.props.workItemData.projects.map(function(item, index) {
          console.log('test : ' + index);
          console.log(item);
          return <Project key={index} projectData={item} />;
        }));
        return projects;
    }
    render() {
      return (
          <div className="workItem">
            <h3>{this.props.workItemData.position}, <span>{this.props.workItemData.company}</span></h3>
            <p className="workDates">{this.getWorkDates()}</p>
            <p>{this.props.workItemData.summary}</p>
            <div className="button-wrapper">
            <Button icon secondary onClick={this._toggleCollapse} className={this.state.className} /> <Button flat secondary onClick={this._toggleCollapse} label="voir tous mes projets" />
            </div>
            <Collapse collapsed={this.state.collapsed}>
              <div className="project-list">{this.getProjects()}</div>
            </Collapse>
          </div>
      )
    }
};

