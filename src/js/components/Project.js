import React from 'react';
import Chip from 'react-md/lib/Chips/Chip';

const Project = props => {

    const getkeywords = props.projectData.technologies.map(function(item, index) {
        return (<Chip key={index} label={<span>{item}</span>} />)
    });

    const projectSize = (props.projectData.size !== '')?' - ' + props.projectData.size:'';

    return (
        <div className="project-item">
          <h4>{props.projectData.name} <span className="size">{projectSize}</span></h4>

          <p className="summary">{props.projectData.summary}</p>
          <p className="role"><strong>Mon rôle</strong> : {props.projectData.role}</p>
          <div className="chip-list">{getkeywords}</div>
        </div>
    )
};

export default Project;