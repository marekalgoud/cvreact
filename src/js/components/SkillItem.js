import React from 'react';
import Chip from 'react-md/lib/Chips/Chip';

const WorkItem = props => {

    const getkeywords = props.skillItemData.keywords.map(function(item, index) {
        return (<Chip key={index} label={<span>{item}</span>} />)
    });

    return (
        <div className="skill-item">
          <div className="chip-list">{getkeywords}</div>
        </div>
    )
};

export default WorkItem;
