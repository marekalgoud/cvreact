import React from 'react';
import PropTypes from 'prop-types';

import Button from 'react-md/lib/Buttons/Button'
import FontIcon from 'react-md/lib/FontIcons'

import Profile from './Profile';
import About from './About';
import Work from './Work';
import Skills from './Skills';
import Education from './Education';
import Interest from './Interest';

const App = props => {
  const profileData = props.jsonObj.basics;
  const aboutData = profileData.summary;
  const workData = props.jsonObj.work;
  const skillsData = props.jsonObj.skills;
  const educationData = props.jsonObj.education;
  const interestsData = props.jsonObj.interests;
  const languageData = props.jsonObj.languages;

  return (
        <div className="wrapper-container">
          <div className="container">
            <div className="md-grid">
              <aside className="aside md-cell md-cell--4 md-background--primary dark">
                <div className="inner">
                  <Profile profileData={profileData} languageData={languageData} />
                </div>
              </aside>
              <main className="md-cell md-cell--8 md-tab-panel">
                  <About aboutData={aboutData} />
                  <Work workData={workData} />
                  <Skills skillsData={skillsData} />
                  <Education educationData={educationData} />
                  <Interest interestsData={interestsData} />
              </main>
              <Button floating fixed className="md-background--secondary" children={<FontIcon>file_download</FontIcon>} onClick={()=>window.open(process.env.PUBLIC_URL + '/cv.pdf')}/>
            </div>
          </div>
        </div>
    )
};

App.propTypes = {
    jsonObj: PropTypes.object.isRequired
}

export default App;