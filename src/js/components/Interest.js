import React from 'react';
import FontIcon from 'react-md/lib/FontIcons'

const Interest = props => {

    const getInterest = () => {
        const interestItems = [];
        props.interestsData.forEach((item, index) => {
          interestItems.push(<div><strong>{item.name}</strong> : {item.details}</div>);
        })
        return interestItems;
    }

    return (
      <section className="interest box">
        <h2 className="md-text-uppercase"><FontIcon>star_border</FontIcon> Centres d'intérêts</h2>
        <div>{getInterest()}</div>
      </section>
    );
};

export default Interest;
