import React from 'react'
import { render } from 'react-dom'

import App from './js/components/App'

import 'roboto-fontface'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'font-awesome/css/font-awesome.min.css'
import './build/css/index.css'
import axios from 'axios';

let json = ''; // load resume file

axios.get('http://localhost:8080/app_dev.php/api/1')
  .then(function (response) {
   json = response.data;
   console.log(json);
   render(
      <App jsonObj={json} />,
      document.getElementById('root')
    );
  })
  .catch(function (error) {
    console.log(error);
  });


